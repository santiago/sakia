import time
import pytest
from sakia.gui.navigation.txhistory.table_model import HistoryTableModel


@pytest.mark.asyncio
async def test_tx_history_table_model(
    application_with_one_connection, fake_server_with_blockchain, bob, alice
):
    application_with_one_connection.instanciate_services()

    bob_connection = application_with_one_connection.db.connections_repo.get_one(
        pubkey=bob.key.pubkey
    )

    date_start = time.time() - 86400
    date_end = time.time()

    history_table_model = HistoryTableModel(
        application_with_one_connection,
        application_with_one_connection,
        bob_connection,
        date_start,
        date_end,
        application_with_one_connection.identities_service,
        application_with_one_connection.transactions_service,
    )

    # send transaction with lock_mode 0
    (
        _,
        sakia_tx_list,
    ) = await application_with_one_connection.documents_service.send_money(
        bob_connection, bob.salt, bob.password, alice.key.pubkey, 100, 0, None, 0, None
    )
    history_table_model.init_transfers()

    transfers = application_with_one_connection.transactions_service.transfers(
        bob.key.pubkey
    )

    # test transfer change
    history_table_model.change_transfer(transfers[0])

    assert len(history_table_model.transfers_data) == 1
    conditions_data = history_table_model.transfers_data[0][
        HistoryTableModel.columns_types.index("conditions")
    ]
    assert conditions_data is None

    # send transaction with lock_mode_1
    (
        _,
        sakia_tx_list,
    ) = await application_with_one_connection.documents_service.send_money(
        bob_connection, bob.salt, bob.password, alice.key.pubkey, 100, 0, None, 1, None
    )
    history_table_model.init_transfers()

    transfers = application_with_one_connection.transactions_service.transfers(
        bob.key.pubkey
    )

    # test transfer change
    history_table_model.change_transfer(transfers[0])

    assert len(history_table_model.transfers_data) == 2
    conditions_data = history_table_model.transfers_data[0][
        HistoryTableModel.columns_types.index("conditions")
    ]
    if conditions_data is None:
        conditions_data = history_table_model.transfers_data[1][
            HistoryTableModel.columns_types.index("conditions")
        ]
    assert (
        conditions_data
        == "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo) || (SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7) && CSV(604800))"
    )

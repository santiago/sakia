import pytest


@pytest.mark.asyncio
async def test_lock_mode_0(application_with_one_connection, fake_server, bob, alice):
    """
    Test the lock mode 0, Receiver signature

    :param application_with_one_connection:
    :param fake_server:
    :param bob:
    :param alice:
    :return:
    """
    # check money amount in bob account
    amount = application_with_one_connection.sources_service.amount(bob.key.pubkey)
    assert amount == 1584

    # create connection to have sources...
    bob_connection = application_with_one_connection.db.connections_repo.get_one(
        pubkey=bob.key.pubkey
    )

    # send transaction
    (
        _,
        sakia_tx_list,
    ) = await application_with_one_connection.documents_service.send_money(
        bob_connection, bob.salt, bob.password, alice.key.pubkey, 100, 0, None, 0, None
    )

    assert len(sakia_tx_list) == 1
    assert (
        "Outputs:\n100:0:SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)\n"
        in sakia_tx_list[0].raw
    )


@pytest.mark.asyncio
async def test_lock_mode_1(application_with_one_connection, fake_server, bob, alice):
    """
    Test the lock mode 0, Receiver signature OR (Sender signature AND CSV(one week delay))

    :param application_with_one_connection:
    :param fake_server:
    :param bob:
    :param alice:
    :return:
    """
    # check money amount in bob account
    amount = application_with_one_connection.sources_service.amount(bob.key.pubkey)
    assert amount == 1584

    # create connection to have sources...
    bob_connection = application_with_one_connection.db.connections_repo.get_one(
        pubkey=bob.key.pubkey
    )

    # send transaction
    (
        _,
        sakia_tx_list,
    ) = await application_with_one_connection.documents_service.send_money(
        bob_connection, bob.salt, bob.password, alice.key.pubkey, 100, 0, None, 1, None
    )

    assert len(sakia_tx_list) == 1
    assert (
        "Outputs:\n100:0:SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo) || (SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7) && CSV(604800))\n"
        in sakia_tx_list[0].raw
    )

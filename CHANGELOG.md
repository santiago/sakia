## v0.52.0 (23/04/2020)
### Features
* #807 Add new "Percentage of Average" referential
* Transfer window now use current referential instead of only relative by UD
* Add current referential units on the balance display
### Bugs
* Fix referentials french translation
* Fix network "Open in browser" menu

## v0.51.1 (05/04/2020)
### Bugs
* #802 Fix "Send as source" context menu not appear in Expert Mode on normal received transactions
* Update incomplete french translation

## v0.51.0 (03/04/2020)
### Features
* #798 - Complete workflow to send transaction that we can get back after one week:
    * A selector allow to choose the lock condition of the output transaction
    * Then, the transaction with special lock condition appears with an underline in the transaction history
    * If you are the receiver, you can use the context menu action "Send as source" to send it to yourself
    before the issuer get it back
    * If you are the issuer, use the context menu action "Send as source" to send it to yourself after the delay
    * Before the delay, you can check the lock condition in the transfer window to see the date when the transaction will be unlocked
    * In Expert Mode (see Preferences), you can use any incoming transaction or dividend as source for your transfer

### Enhancement
* Add a legend for colors, italic and underlined display of transactions under transaction history
* Upgrade dependencies to duniterpy 0.57.0

### Bugs
* Fix gen_translations.py running Qt4 lrelease in Makefile
* Fix error parsing some WSP2 Head message because software version had a prefix (duniterpy fix)

### CI/CD
* Remove obsolete runner tag
* Release job only on `master`, release_test job only on `dev` or `tags`

## v0.50.5 (21/03/2020)
### Bugs
* Fix send money to pubkey with search user selected

### Tests
* Update and fix pytest suite
* Update duniter-mirage dependency version

### Enhancements
* Tx history update button set to disable until update is done
* Increase delay between network crawlings to 1 mn

### CI/CD
* Add format and tests stage in .gitlab-ci.yml

## v0.50.4 (18/03/2020)
### Code
* Fix session closed error (regression in v0.50.3)

## v0.50.3 (16/03/2020)
### Code
* Fix exception when refresh tx history
* Fix Unclosed client session error (at last !)
### CI/CD
* Fix bug with setuptools 28.8.x version in gitlab CI/CD image

## v0.50.2 (10/03/2020)
### Code
* Add system libs requirement and command line options in README.md
* Fix bad label "ok" on startup message box and enhance it
* Propose new sakia account menu at first start instead of create a new member account on the network
* Only display licence step when registering a new member account on the network
* Update french translation
### Project
* Fix release.sh using simple quote on sakia.__init__ version
* Add this changelog

## v0.50.1 (08/03/2020)
### Code
* Use asyncio.ensure_future() instead of async() which disappear from Py 3.7
* Include the license file into the wheel distributed, the license changed name
### CI/CD
* Fix sources path and allow all file extensions to trigger the CI
### Build
* Add dist check after build in Makefile
### Project
* Fix release.sh broken by black format

## [v0.50.0](https://git.duniter.org/clients/python/sakia/-/milestones/15) (07/03/2020)
### Code
* Update DuniterPy dependency to 0.56.0
* Fix all detected bugs
* Add the Sakia icon in toolbar
* Remove search direct connections
* Add update button in tx history
* Fix translation system
* Update french translation
